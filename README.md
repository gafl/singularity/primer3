# primer3 Singularity container
Bionformatics package primer3<br>
Design PCR primers from DNA sequence. From mispriming libraries to sequence quality data to the generation of internal oligos, primer3 does it.
https://github.com/primer3-org/primer3
primer3 Version: 2.5.0<br>

Singularity container based on the recipe: Singularity.primer3_v2.5.0

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull primer3_v2.5.0.sif oras://registry.forgemia.inra.fr/gafl/singularity/primer3/primer3:latest


